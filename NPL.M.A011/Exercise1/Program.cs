﻿using Exercise1;
using System.Collections;
using System.Text;

internal class Program
{
    private static void Main(string[] args)
    {

        Console.OutputEncoding = Encoding.Unicode;

        ArrayList arrayList = new ArrayList();
        arrayList.Add("Hieu");
        arrayList.Add("Hoang");
        arrayList.Add(1);
        arrayList.Add(2);
        arrayList.Add("Trong");
        arrayList.Add(3.9d);

        ArrayListExtension extension = new ArrayListExtension();

        int countInt = extension.CountInt(arrayList);
        int countOfString = extension.CountOf(arrayList, typeof(string));
        int countIntGeneric = extension.CountOf<int>(arrayList);
        int maxInt = extension.MaxOf<int>(arrayList);

        Console.WriteLine("CountInt() should return: " + countInt);
        Console.WriteLine("CountOf(typeof(string)) should return: " + countOfString);
        Console.WriteLine("CountOf<int>() should return: " + countIntGeneric);
        Console.WriteLine("MaxOf<int>() should return: " + maxInt);



        int countIntStatic = arrayList.CountInt();
        int countOfStringStatic = arrayList.CountOf(typeof(string));
        int countIntGenericStatic = arrayList.CountOf<int>();
        int maxIntStatic = arrayList.MaxOf<int>();

        Console.WriteLine("CountInt() should return: " + countIntStatic);
        Console.WriteLine("CountOf(typeof(string)) should return: " + countOfStringStatic);
        Console.WriteLine("CountOf<int>() should return: " + countIntGenericStatic);
        Console.WriteLine("MaxOf<int>() should return: " + maxIntStatic);
    }
}