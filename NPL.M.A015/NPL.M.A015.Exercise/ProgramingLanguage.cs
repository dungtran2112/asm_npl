﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise_
{
    internal class ProgramingLanguage
    { 
        public int Id { get; set; }
        public string LanguageName {  get; set; }

        public ProgramingLanguage(int id, string languageName)
        {
            Id = id;
            LanguageName = languageName;
        }

        public ProgramingLanguage()
        {
        }
    }
}
