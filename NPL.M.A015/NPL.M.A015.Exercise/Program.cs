﻿
using NPL.M.A015.Exercise_;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("======= LINQ Practice programm =======");

        List<ProgramingLanguage> LanguageList = new List<ProgramingLanguage>
        {
            new ProgramingLanguage(1, "C"),
            new ProgramingLanguage(2, "C++"),
            new ProgramingLanguage(3, "Java"),
            new ProgramingLanguage(4, "C#")
        };
        List<Department> DepartmentList = new List<Department>
        {
            new Department(1, "Department 1"),
            new Department(2, "Department 2"),
            new Department(3, "Department 3"),
            new Department(4, "Department 4"),
            new Department(5, "Department 5"),
            new Department(6, "Department 6")
        };
        List<Employee> EmployeeList = new List<Employee>
        {
            new Employee(01, "Pham Khai Van", 21, "Thanh pho Bac Ninh", DateTime.Now, true, 1),
            new Employee(02, "Tran Hong Ngoc", 21, "Ha Noi", DateTime.Now, true, 2),
            new Employee(03, "Le Thuy Dung", 21, "Yen Phong", DateTime.Now, false, 3),
            new Employee(04, "Tran Ha Ngoc Thao", 21, "Thanh pho Bac Ninh", DateTime.Now, true, 5),
            new Employee(05, "Ngo Thi Thanh Lam", 21, "Tien Du", DateTime.Now, true, 3),
        };
        List<(int, int)> EmployeeLanguage = new List<(int, int)>
        {
            (01, 1), //C
            (01, 3), //Java
            (01, 4), //C#
            (03, 1), //C
            (03, 2), //C++
            (03, 3), //Java
            (04, 1), //C
            (04, 2)  //C++
        };

        Management manager = new Management(EmployeeList, LanguageList , DepartmentList, EmployeeLanguage);



        int choice;
        do
        {
            Console.WriteLine("\tMenu options:");
            Console.WriteLine("1. Print out departments which have the number of employees greater than or equal an integer.");
            Console.WriteLine("2. Get list of all employees are working.");
            Console.WriteLine("3. Get list of all employees know this programing language.");
            Console.WriteLine("4. Get languages that an employee knows");
            Console.WriteLine("5. Get list of employees who know multiple programming languages.");
            Console.WriteLine("6. Get Employee Paging.");
            Console.WriteLine("7. Get all departments including employees that belong to each department.");
            Console.WriteLine("8. ");
            Console.WriteLine("9. ");
            Console.WriteLine("10. ");
            Console.WriteLine("11. ");
            Console.WriteLine("0. Exit");
            choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 0:
                    Console.WriteLine("Exit program!");
                    break;
                case 1:
                    Console.WriteLine("input number of employee :");
                    List<Department> dep1 =manager.GetDepartments(Convert.ToInt32(Console.ReadLine()));
                    foreach (Department dep in dep1)
                    {
                        Console.WriteLine(dep.DepartmentId +"   " + dep.DepartmentName);
                    }
                    
                    break;
                case 2:
                    List<Employee> emp1 = manager.GetEmployeesWorking();

                    foreach (Employee emp in emp1)
                    {
                        Console.WriteLine(emp.EmployeeName);
                    }
                    break;
                case 3:

                    Console.WriteLine("Name:");
                    List<Employee> emp2 = manager.GetEmployees(Console.ReadLine());
                    foreach (Employee emp in emp2)
                    {
                        Console.WriteLine(emp.EmployeeName);
                    }
                    break;
                case 4:
                   
                    break;
                case 5:
                    
                    break;
                case 6:
                    
                    break;
                case 7:
                   
                    break;
                case 8:
                    Console.WriteLine();
                    break;
                case 9:
                    Console.WriteLine();
                    break;

            }
        } while (choice != 0);

    }
}