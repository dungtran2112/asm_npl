﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise_
{
    internal class Management
    {
        public List<Employee> employees = new List<Employee>();
        public List<ProgramingLanguage> programingLanguages = new List<ProgramingLanguage>();
        public List<Department> department = new List<Department>();
        public List<(int Id, int EmployeeId)> EmProgram = new List<(int Id, int EmployeeId)>();

        public Management(List<Employee> employees, List<ProgramingLanguage> programingLanguages, List<Department> department, List<(int Id, int EmployeeId)> emProgram)
        {
            this.employees = employees;
            this.programingLanguages = programingLanguages;
            this.department = department;
            EmProgram = emProgram;
        }

        public List<Department> GetDepartments(int numberOfEmployees)
        {
            var soluong = from emp in employees
                           group emp by emp.DepartmentId into van

                           select new
                           {
                               cbm = van.Key,
                               soluong = van.Count()
                           };


            var a = (from dep in department
                           join cbm in soluong on dep.DepartmentId equals cbm.cbm
                           where cbm.soluong >= numberOfEmployees
                           select dep).ToList();




            return a;
        }

        public List<Employee> GetEmployeesWorking()
        {
            List<Employee> tmp = new List<Employee>();
            var res = (from emp in employees
                       where emp.Status == true
                       join dep in department on emp.DepartmentId equals dep.DepartmentId
                       select emp).ToList() ;

            

            return res;
        }


        public List<Employee> GetEmployees(string languageName)
        {
            List<Employee> res = new List<Employee>();
            ProgramingLanguage language = (from prl in programingLanguages
                                          where prl.LanguageName == languageName
                                          select prl).FirstOrDefault();

            var tmp = (from emp in employees
                      join x in EmProgram on emp.EmployeeId equals x.Item1
                      where x.Id == language.Id
                      select emp).ToList() ;
          
            return tmp;
        }




    }
}
