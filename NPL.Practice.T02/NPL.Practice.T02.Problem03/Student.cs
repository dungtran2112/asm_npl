﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    internal class Student: IGraduate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public decimal SqlMark { get; set; }
        public decimal CsharpMark { get; set; }
        public decimal DsaMark { get; set; }
        public decimal GPA { get; set; }
        public GraduateLevel GraduateLevel { get; set; }

        public void Graduate()
        {
            
            GPA = (SqlMark + CsharpMark + DsaMark) / 3;
             GPA = Math.Round( GPA,2 );
            if (GPA >= 9) 
            {
                GraduateLevel = (GraduateLevel)0;
            }
            else if(GPA >= 8)
            {
                GraduateLevel = (GraduateLevel)1;
            }
            else if(GPA >= 7)
            {
                GraduateLevel = (GraduateLevel)2;
            }
            else if(GPA >= 5)
            {
                GraduateLevel = (GraduateLevel)3;
            }
            else
            {
                GraduateLevel = (GraduateLevel)4;
            }
            
        }
        public String GetCertificate()
        {
          return ( "Name: "+Name + ", SqlMark: " + SqlMark+ ", CsharpMark: " + CsharpMark+ ",DsaMark: " + DsaMark + ",GPA: " + GPA + ",GraduateLevel: " + GraduateLevel);
        }
    }
    enum GraduateLevel {
        Excellent = 0, VeryGood, Good, Average, Failed
    }

}
