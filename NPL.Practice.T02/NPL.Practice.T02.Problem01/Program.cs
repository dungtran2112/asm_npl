﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.WriteLine("mời bạn nhập : ");
        string Input = Console.ReadLine();
        Console.WriteLine("số từ tối đa trong văn bản tóm tắt :");
        int summary = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine(GetArticleSummary(Input, summary));

    }

    static public string GetArticleSummary(string content, int maxLength)
    {
        if(content.Length > maxLength)
        {
            int index = 50;
            //for (int i = maxLength; i >0; i--)
            //{
            //    if (content[i] == ' ') 
            //    {
            //        index = i; 
            //        break;
            //    }
            //}
            index = content.Substring(0, maxLength).LastIndexOf(" ");
            return content.Substring(0, index) + "...";
            
        }
        else
        {
            return content;
        }

    }

}