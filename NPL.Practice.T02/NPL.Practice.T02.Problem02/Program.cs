﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.WriteLine("nhập số phần tử của mảng cho anh :");
        int inputArray = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("nhập số phần tử của mảng con cho anh :");
        int subLength = Convert.ToInt32(Console.ReadLine());
        int[] arr = new int[inputArray];
        for(int i = 0; i < inputArray; i++)
        {
            arr[i] = Convert.ToInt32(Console.ReadLine());
        }

        Console.WriteLine(FindMaxSubArray(arr, subLength));

    }

    static public int FindMaxSubArray(int[] inputArray, int subLength)
    {
        if(inputArray.Length < subLength)
        {
            throw new ArgumentException(" độ dài mảng con lớn hơn mảng cha ");

        }

        int sum = 0;
        for (int i = 0; i < subLength; i++)
        {
            sum += inputArray[i];
        }
        int maxSum = sum;
        for(int i = subLength; i < inputArray.Length; i++)
        {
            sum = sum - inputArray[i-subLength] + inputArray[i];
            if(sum > maxSum)
            {
                maxSum = sum;
            }
        }

    return maxSum;
    }
}